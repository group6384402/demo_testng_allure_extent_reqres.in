package Repository;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.DataProvider;

import Common_Methods.Utility;

public class RequestBody extends Environment {

	public static String req_post_tc(String TestCaseName) throws IOException {
		ArrayList<String> Data = Utility.readExcelData("Post_API", TestCaseName);
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);
		String req_body = "{\r\n" + "    \"" + key_name + "\": \"" + value_name + "\",\r\n" + "    \"" + key_job
				+ "\": \"" + value_job + "\"\r\n" + "}";

		return req_body;
	}

	@DataProvider()
	public Object[][] body_create() {
		return new Object[][] { { "morpheus", "leader" }, { "jayesh", "qa" }, { "jon", "ba" }, { "swapneel", "da" } };
	}

	@DataProvider()
	public Object[][] body_login() {
		return new Object[][] { { "eve.holt@reqres.in", "cityslicka" }, { "eve.holt@reqres.in", "pistol" } };
	}

	@DataProvider()
	public Object[][] body_nologin() {
		return new Object[][] { { "cityslicka", "" },
				// { "", "pistol" }
		};
	}

	@DataProvider()
	public Object[][] body_nouser() {
		return new Object[][] { { "", "pistol" } };
	}

	@DataProvider()
	public Object[][] body_delete() {
			return new Object[][] {
					{"api/users/2"},
					{"api/users/2"}
					};
	}

	@DataProvider()
	public Object[][] body_get_users_page2() {
		return new Object[][] { { "api/users?page=2" }
		};
		}
		
		@DataProvider()
		public Object[][] body_get_users_page1() {
			return new Object[][] {  { "api/users?page=1" }  };
	}
}
