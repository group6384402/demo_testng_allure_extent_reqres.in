package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_9 extends RequestBody {

	String requestBody;
	String Endpoint;
	File dir_name;
	Response response;
	int statuscode;

	@BeforeTest
	public void testSetUp() throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		Endpoint = Environment.Hostname() + Environment.Resource_register();
	}

	@Test(dataProvider = "body_login", dataProviderClass = Repository.RequestBody.class, description = "Data_Provider_Different_Class_Test")
	public void validator(String Req_name, String Req_job) throws IOException {

		requestBody = "{\r\n" + "    \"email\": \"" + Req_name + "\",\r\n" + "    \"password\": \"" + Req_job + "\"\r\n"
				+ "}";

		System.out.println(RequestBody.HeaderName());
		System.out.println(RequestBody.HeaderValue());
		System.out.println(requestBody);
		System.out.println(Endpoint);

		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);

		statuscode = response.statusCode();

		ResponseBody res_body = response.getBody();
		String res_id = res_body.jsonPath().getString("id");
		String res_token = res_body.jsonPath().getString("token");

		// Validate the response parameters
		Assert.assertEquals(statuscode, 200);
		Assert.assertNotEquals(res_id, null);
		Assert.assertNotNull(res_token);

		// Assert.assertEquals(res_createdAt, expecteddate);

		// Evidence Creation
		Utility.evidenceFileCreator(Utility.testLogName("Post_register"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

	@AfterTest
	public void evidenceCreator() {
	}
}
