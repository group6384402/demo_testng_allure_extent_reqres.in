package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_AA extends RequestBody {

	String requestBody;
	String Endpoint;
	File dir_name;
	Response response;
	int statuscode = 0;

	@Test(dataProvider = "body_get_users_page1", dataProviderClass = Repository.RequestBody.class, description = "Data_Provider_Different_Class_Test")
	public void validator(String Resource) throws IOException {

		dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		Endpoint = Environment.Hostname() + Resource;

		Response response = API_Trigger.Get_trigger(Endpoint);
		statuscode = response.statusCode();

		Utility.evidenceFileCreator(Utility.testLogName("Get_users_2"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		ResponseBody res_body = response.getBody();

		// Step 5.2 : Parse individual params using jsp_res object 
        String res_id=res_body.jsonPath().getString("data.id");
		String res_email=res_body.jsonPath().getString("data.email");
		String res_first_name=res_body.jsonPath().getString("data.first_name");
		String res_last_name=res_body.jsonPath().getString("data.last_name");
		String res_avatar=res_body.jsonPath().getString("data.avatar");
		
		
		//step  6.0 store expected result
		String exp_id="[1, 2, 3, 4, 5, 6]";
		String exp_email="[george.bluth@reqres.in, janet.weaver@reqres.in, emma.wong@reqres.in, eve.holt@reqres.in, charles.morris@reqres.in, tracey.ramos@reqres.in]";
		String exp_first_name="[George, Janet, Emma, Eve, Charles, Tracey]";
		String exp_last_name="[Bluth, Weaver, Wong, Holt, Morris, Ramos]";
		String exp_avatar="[https://reqres.in/img/faces/1-image.jpg, https://reqres.in/img/faces/2-image.jpg, https://reqres.in/img/faces/3-image.jpg, https://reqres.in/img/faces/4-image.jpg, https://reqres.in/img/faces/5-image.jpg, https://reqres.in/img/faces/6-image.jpg]";
				 
		
		// Step 6 : Validate the response body

		// Step 6.2 : Use TestNG's Assert   
		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(res_id, exp_id);
		Assert.assertEquals(res_email,exp_email );
		Assert.assertEquals(res_first_name, exp_first_name);
		Assert.assertEquals(res_last_name, exp_last_name);
		Assert.assertEquals(res_avatar, exp_avatar);
	}

}
