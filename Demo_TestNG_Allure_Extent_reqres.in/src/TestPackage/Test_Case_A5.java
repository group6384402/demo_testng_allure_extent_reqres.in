package TestPackage;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
import io.restassured.response.Response;

public class Test_Case_A5 extends RequestBody {

	String requestBody;
	String Endpoint;
	File dir_name;
	Response response;
	int statuscode = 0;

	@Test(dataProvider = "body_delete", dataProviderClass = Repository.RequestBody.class, description = "Data_Provider_Different_Class_Test")
	public void validator(String Resource) throws IOException {
		dir_name = Utility.CreateLogDirectory("Delete_API_Logs");
		String Endpoint = Environment.Hostname() + Resource;
		System.out.println(Endpoint);

		Response response = API_Trigger.Delete_trigger(Endpoint);

		statuscode = response.statusCode();

		Utility.evidenceFileCreator(Utility.testLogName("Delete_users"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

		Assert.assertEquals(statuscode, 204);
	}

}
