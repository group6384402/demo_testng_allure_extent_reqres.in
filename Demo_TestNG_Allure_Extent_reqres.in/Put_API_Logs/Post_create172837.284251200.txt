Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 11:58:37 GMT

Response body is : 
{"name":"morpheus","job":"leader","updatedAt":"2024-03-08T11:58:37.528Z"}