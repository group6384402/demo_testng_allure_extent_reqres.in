Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "swapneel",
    "job": "da"
}

Response header date is : 
Fri, 08 Mar 2024 11:58:39 GMT

Response body is : 
{"name":"swapneel","job":"da","updatedAt":"2024-03-08T11:58:38.970Z"}