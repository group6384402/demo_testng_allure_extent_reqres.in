Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "jon",
    "job": "ba"
}

Response header date is : 
Fri, 08 Mar 2024 11:58:42 GMT

Response body is : 
{"name":"jon","job":"ba","id":"971","createdAt":"2024-03-08T11:58:42.853Z"}