**Demo_TestNG_Allure_Extent_reqres.in**

In this project I have built a TestNG framework to validate http methods(post,put,patch,delete,get) and generate user freindly reports using the allure and extent frameworks

This framework uses @BeforeTest
                    @Test
                    @AfterTest
                    @DataProvider annotations from TestNG

The Demonstration is done using reqres.in APIs

**Prerequisites**

eclipse IDE


**Features**

1) TestNG
2) allure reports
3) extent reports
4) DataProvider


To use this framework clone this at your favourite directory and directly import in the eclipse using open projects from filesystem

right click on pom.xml-> MAVEN-> UPDATE Project
right click on TestNG.xml->run as-> TestNG Suite

for allure reports go to the project directory and enter the command->allure serve ./allure-results
it will build the report and open it in your default web browser

for exent reports refresh the project after the test and expand the extent-report folder you will see report.html, double click on it,report will open in your default web browser
